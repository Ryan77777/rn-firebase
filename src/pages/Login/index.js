import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';

const Login = ({navigation}) => {
  const [initializing, setInitializing] = useState(true);

  const [form] = useState({
    fullName: 'Ryan',
    profession: 'Programmer',
    email: 'Ryan.Aprianto77777@gmail.com',
    password: '12324535',
  });

  function onAuthStateChanged(user) {
    if (user) {
      navigation.replace('Homepage')
    }
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber;
  }, []);

  if (initializing) return null;

  const onRegister = () => {
    auth()
      .createUserWithEmailAndPassword(form.email, form.password)
      .then(res => {
        const data = {
          fullName: form.fullName,
          profession: form.profession,
          email: form.email,
          uid: res.user.uid,
        };

        database()
          .ref('/users' + res.user.uid + '/')
          .set(data);
        navigation.replace('Homepage');
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!');
        }

        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }

        console.log(error);
      });
  };

  const onLogin = () => {
    auth()
      .signInWithEmailAndPassword(form.email, form.password)
      .then(res => {
          navigation.replace('Homepage');
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!');
        }

        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }
      });
  };

  return (
    <View style={styles.page}>
      <TouchableOpacity onPress={onRegister}>
        <Text>Register</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={onLogin}>
        <Text>Login</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
